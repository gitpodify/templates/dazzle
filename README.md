# A Dazzle template on Gitpod

This is a [Dazzle](https://github.com/gitpod-io/dazzle) template configured for ephemeral development environments on [Gitpod](https://www.gitpod.io/).

While this repository serves as an baseline for Dazzle-based Dockerfile projects, we gone an extra mile by:

* Providing an Bash script to seamlessly run local registry. Set `USE_DOCKER_COMPOSE` to use Docker Compose with default configurations
  or see [`config/README.md`](./config/README.md) for specifics.
* An direnv rc file to provide utility variables and bring commands from `scripts` directory into PATH, if allowed through `direnv allow`.
* Some GitLab CI configuration to help build them, especially on GitLab SaaS shared runners.

## Next Steps

Click the button below to start a new development environment:

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/gitpodify/templates/vnc)

## Get Started With Your Own Project

### A new project

Click the above "Open in Gitpod" button to start a new workspace. Once you're ready to push your first code changes, Gitpod will guide you to fork this project so you own it.

### An existing project

To get started with x11vnc on Gitpod, add a [`.gitpod.yml`](./.gitpod.yml) file which contains the configuration to improve the developer experience on Gitpod. To learn more, please see the [Getting Started](https://www.gitpod.io/docs/getting-started) documentation.

## Notes & caveats

* This template uses direnv to bring scripts in `scripts` directory to PATH, among other magic such as automagically set `REPO_ROOT` variable.
* This template uses `quay.io/gitpodified-workspace-images/full` instead of the usual `gitpod/workspace-full` as the workspace image. If you're curious, sources are available at <https://gitlab.com/gitpodify/gitpodified-workspace-images>.
