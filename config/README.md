# Configuration Directory

This directory holds config files for some scripts included in <https://gitlab.com/gitpodify/templates/dazzle>.

* Use an compose file for local registry:

  ```bash
  # use default config
  USE_DOCKER_COMPOSE=true ./scripts/run-local-registry

  # have an customizations or want to use another template?
  echo "$REPO_ROOT/.gitpod/storj-dcs-backend.docker-compose.yml" >> config/custom-compose-file
  export STORJ_S3_GATEWAY_REGION=eu1 STORJ_S3_GATEWAY_ACCESS_KEY=<access key> STORJ_S3_GATEWAY_SECRET_KEY=<secret key>
  USE_DOCKER_COMPOSE=true ./scripts/run-local-registry
  ```
