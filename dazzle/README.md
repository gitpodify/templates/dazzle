# Dazzle chunks, tests and base image files

This directory is where your chunks, tests and even the base Dockerfile are being managed by Dazzle. You can edit `dazzle.yaml`
to customize how Dazzle builds and combines chunks into an ready-to-bake Docker images for different purposes.

## Cheat Sheet

This section is currently work in progress, but the essiental commands and flags are there for most use cases. Feel free to submit merge requests to improve this.

> **Tip**: Instead of manually add `--addr` and `--context` global flags to your Dazzle command calls, use the `dazzle-in-glci`
script to handle them via environment variables (`DAZZLE_CONTEXT_DIR` and `BUILDKIT_HOST`).
If you're in Gitpod/local devenv/Remote Containers, create an `.env` file, add these
variables and don't forget to `direnv reload` it.

### Project Management

* Setup an freshly-baked Dazzle project: `dazzle project init`
* Add an new chunk: `dazzle project init <new-chunk-name>`
* Add some chunk combinations: `dazzle project add-combination <preferred-combination-name> <chunk> [chunk ...]`

### Building, Managing and Merging Chunks

**Hint**: `<target-ref>` refers to the image repository path on the remote. We'll use our local registry at `localhost:5000` as the registry host.

* Build image for an specific chunk: `dazzle build <target-ref>`
* Build and combine chunks: `dazzle combine <target-ref> --combination <combo-name>`
* Build all combinations: `dazzle combine <target-ref> --all`